package com.mukesh.countrypicker.listeners;

import ohos.agp.components.Component;

public interface BottomSheetInteractionListener {

    void initiateUi(Component component);

    void setCustomStyle(Component component);

    void setSearchEditText();

    void setupRecyclerView(Component component);
}
