package com.mukesh.countrypicker;

import com.mukesh.countrypicker.listeners.BottomSheetInteractionListener;
import com.mukesh.countrypicker.listeners.OnCountryPickerListener;
import com.mukesh.countrypicker.listeners.OnItemClickListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.Lifecycle;
import ohos.aafwk.ability.LifecycleStateObserver;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;
import ohos.telephony.SimInfoManager;
import ohos.telephony.TelephonyConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class CountryPicker implements BottomSheetInteractionListener, LifecycleStateObserver {
    private static final String TAG = "CountryPicker";
    // region Countries
    private final Country[] COUNTRIES = {
            new Country("AD", "Andorra", "+376", ResourceTable.Media_flag_ad, "EUR"),
            new Country("AE", "United Arab Emirates", "+971", ResourceTable.Media_flag_ae, "AED"),
            new Country("AF", "Afghanistan", "+93", ResourceTable.Media_flag_af, "AFN"),
            new Country("AG", "Antigua and Barbuda", "+1-268", ResourceTable.Media_flag_ag, "XCD"),
            new Country("AI", "Anguilla", "+1-264", ResourceTable.Media_flag_ai, "XCD"),
            new Country("AL", "Albania", "+355", ResourceTable.Media_flag_al, "ALL"),
            new Country("AM", "Armenia", "+374", ResourceTable.Media_flag_am, "AMD"),
            new Country("AN", "Netherlands Antilles", "+599", ResourceTable.Media_flag_an, "ANG"),
            new Country("AO", "Angola", "+244", ResourceTable.Media_flag_ao, "AOA"),
            new Country("AQ", "Antarctica", "+672", ResourceTable.Media_flag_aq, "USD"),
            new Country("AR", "Argentina", "+54", ResourceTable.Media_flag_ar, "ARS"),
            new Country("AS", "American Samoa", "+1-684", ResourceTable.Media_flag_as, "USD"),
            new Country("AT", "Austria", "+43", ResourceTable.Media_flag_at, "EUR"),
            new Country("AU", "Australia", "+61", ResourceTable.Media_flag_au, "AUD"),
            new Country("AW", "Aruba", "+297", ResourceTable.Media_flag_aw, "AWG"),
            new Country("AZ", "Azerbaijan", "+994", ResourceTable.Media_flag_az, "AZN"),
            new Country("BA", "Bosnia and Herzegovina", "+387", ResourceTable.Media_flag_ba, "BAM"),
            new Country("BB", "Barbados", "+1-246", ResourceTable.Media_flag_bb, "BBD"),
            new Country("BD", "Bangladesh", "+880", ResourceTable.Media_flag_bd, "BDT"),
            new Country("BE", "Belgium", "+32", ResourceTable.Media_flag_be, "EUR"),
            new Country("BF", "Burkina Faso", "+226", ResourceTable.Media_flag_bf, "XOF"),
            new Country("BG", "Bulgaria", "+359", ResourceTable.Media_flag_bg, "BGN"),
            new Country("BH", "Bahrain", "+973", ResourceTable.Media_flag_bh, "BHD"),
            new Country("BI", "Burundi", "+257", ResourceTable.Media_flag_bi, "BIF"),
            new Country("BJ", "Benin", "+229", ResourceTable.Media_flag_bj, "XOF"),
            new Country("BL", "Saint Barthelemy", "+590", ResourceTable.Media_flag_bl, "EUR"),
            new Country("BM", "Bermuda", "+1-441", ResourceTable.Media_flag_bm, "BMD"),
            new Country("BN", "Brunei", "+673", ResourceTable.Media_flag_bn, "BND"),
            new Country("BO", "Bolivia", "+591", ResourceTable.Media_flag_bo, "BOB"),
            new Country("BR", "Brazil", "+55", ResourceTable.Media_flag_br, "BRL"),
            new Country("BS", "Bahamas", "+1-242", ResourceTable.Media_flag_bs, "BSD"),
            new Country("BT", "Bhutan", "+975", ResourceTable.Media_flag_bt, "BTN"),
            new Country("BW", "Botswana", "+267", ResourceTable.Media_flag_bw, "BWP"),
            new Country("BY", "Belarus", "+375", ResourceTable.Media_flag_by, "BYR"),
            new Country("BZ", "Belize", "+501", ResourceTable.Media_flag_bz, "BZD"),
            new Country("CA", "Canada", "+1", ResourceTable.Media_flag_ca, "CAD"),
            new Country("CC", "Cocos Islands", "+61", ResourceTable.Media_flag_cc, "AUD"),
            new Country("CD", "Democratic Republic of the Congo", "+243", ResourceTable.Media_flag_cd, "CDF"),
            new Country("CF", "Central African Republic", "+236", ResourceTable.Media_flag_cf, "XAF"),
            new Country("CG", "Republic of the Congo", "+242", ResourceTable.Media_flag_cg, "XAF"),
            new Country("CH", "Switzerland", "+41", ResourceTable.Media_flag_ch, "CHF"),
            new Country("CI", "Ivory Coast", "+225", ResourceTable.Media_flag_ci, "XOF"),
            new Country("CK", "Cook Islands", "+682", ResourceTable.Media_flag_ck, "NZD"),
            new Country("CL", "Chile", "+56", ResourceTable.Media_flag_cl, "CLP"),
            new Country("CM", "Cameroon", "+237", ResourceTable.Media_flag_cm, "XAF"),
            new Country("CN", "China", "+86", ResourceTable.Media_flag_cn, "CNY"),
            new Country("CO", "Colombia", "+57", ResourceTable.Media_flag_co, "COP"),
            new Country("CR", "Costa Rica", "+506", ResourceTable.Media_flag_cr, "CRC"),
            new Country("CU", "Cuba", "+53", ResourceTable.Media_flag_cu, "CUP"),
            new Country("CV", "Cape Verde", "+238", ResourceTable.Media_flag_cv, "CVE"),
            new Country("CW", "Curacao", "+599", ResourceTable.Media_flag_cw, "ANG"),
            new Country("CX", "Christmas Island", "+61", ResourceTable.Media_flag_cx, "AUD"),
            new Country("CY", "Cyprus", "+357", ResourceTable.Media_flag_cy, "EUR"),
            new Country("CZ", "Czech Republic", "+420", ResourceTable.Media_flag_cz, "CZK"),
            new Country("DE", "Germany", "+49", ResourceTable.Media_flag_de, "EUR"),
            new Country("DJ", "Djibouti", "+253", ResourceTable.Media_flag_dj, "DJF"),
            new Country("DK", "Denmark", "+45", ResourceTable.Media_flag_dk, "DKK"),
            new Country("DM", "Dominica", "+1-767", ResourceTable.Media_flag_dm, "XCD"),
            new Country("DO", "Dominican Republic", "+1-809, +1-829, +1-849", ResourceTable.Media_flag_do, "DOP"),
            new Country("DZ", "Algeria", "+213", ResourceTable.Media_flag_dz, "DZD"),
            new Country("EC", "Ecuador", "+593", ResourceTable.Media_flag_ec, "USD"),
            new Country("EE", "Estonia", "+372", ResourceTable.Media_flag_ee, "EUR"),
            new Country("EG", "Egypt", "+20", ResourceTable.Media_flag_eg, "EGP"),
            new Country("EH", "Western Sahara", "+212", ResourceTable.Media_flag_eh, "MAD"),
            new Country("ER", "Eritrea", "+291", ResourceTable.Media_flag_er, "ERN"),
            new Country("ES", "Spain", "+34", ResourceTable.Media_flag_es, "EUR"),
            new Country("ET", "Ethiopia", "+251", ResourceTable.Media_flag_et, "ETB"),
            new Country("FI", "Finland", "+358", ResourceTable.Media_flag_fi, "EUR"),
            new Country("FJ", "Fiji", "+679", ResourceTable.Media_flag_fj, "FJD"),
            new Country("FK", "Falkland Islands", "+500", ResourceTable.Media_flag_fk, "FKP"),
            new Country("FM", "Micronesia", "+691", ResourceTable.Media_flag_fm, "USD"),
            new Country("FO", "Faroe Islands", "+298", ResourceTable.Media_flag_fo, "DKK"),
            new Country("FR", "France", "+33", ResourceTable.Media_flag_fr, "EUR"),
            new Country("GA", "Gabon", "+241", ResourceTable.Media_flag_ga, "XAF"),
            new Country("GB", "United Kingdom", "+44", ResourceTable.Media_flag_gb, "GBP"),
            new Country("GD", "Grenada", "+1-473", ResourceTable.Media_flag_gd, "XCD"),
            new Country("GE", "Georgia", "+995", ResourceTable.Media_flag_ge, "GEL"),
            new Country("GG", "Guernsey", "+44-1481", ResourceTable.Media_flag_gg, "GGP"),
            new Country("GH", "Ghana", "+233", ResourceTable.Media_flag_gh, "GHS"),
            new Country("GI", "Gibraltar", "+350", ResourceTable.Media_flag_gi, "GIP"),
            new Country("GL", "Greenland", "+299", ResourceTable.Media_flag_gl, "DKK"),
            new Country("GM", "Gambia", "+220", ResourceTable.Media_flag_gm, "GMD"),
            new Country("GN", "Guinea", "+224", ResourceTable.Media_flag_gn, "GNF"),
            new Country("GQ", "Equatorial Guinea", "+240", ResourceTable.Media_flag_gq, "XAF"),
            new Country("GR", "Greece", "+30", ResourceTable.Media_flag_gr, "EUR"),
            new Country("GT", "Guatemala", "+502", ResourceTable.Media_flag_gt, "GTQ"),
            new Country("GU", "Guam", "+1-671", ResourceTable.Media_flag_gu, "USD"),
            new Country("GW", "Guinea-Bissau", "+245", ResourceTable.Media_flag_gw, "XOF"),
            new Country("GY", "Guyana", "+592", ResourceTable.Media_flag_gy, "GYD"),
            new Country("HK", "Hong Kong", "+852", ResourceTable.Media_flag_hk, "HKD"),
            new Country("HN", "Honduras", "+504", ResourceTable.Media_flag_hn, "HNL"),
            new Country("HR", "Croatia", "+385", ResourceTable.Media_flag_hr, "HRK"),
            new Country("HT", "Haiti", "+509", ResourceTable.Media_flag_ht, "HTG"),
            new Country("HU", "Hungary", "+36", ResourceTable.Media_flag_hu, "HUF"),
            new Country("ID", "Indonesia", "+62", ResourceTable.Media_flag_id, "IDR"),
            new Country("IE", "Ireland", "+353", ResourceTable.Media_flag_ie, "EUR"),
            new Country("IL", "Israel", "+972", ResourceTable.Media_flag_il, "ILS"),
            new Country("IM", "Isle of Man", "+44-1624", ResourceTable.Media_flag_im, "GBP"),
            new Country("IN", "India", "+91", ResourceTable.Media_flag_in, "INR"),
            new Country("IO", "British Indian Ocean Territory", "+246", ResourceTable.Media_flag_io, "USD"),
            new Country("IQ", "Iraq", "+964", ResourceTable.Media_flag_iq, "IQD"),
            new Country("IR", "Iran", "+98", ResourceTable.Media_flag_ir, "IRR"),
            new Country("IS", "Iceland", "+354", ResourceTable.Media_flag_is, "ISK"),
            new Country("IT", "Italy", "+39", ResourceTable.Media_flag_it, "EUR"),
            new Country("JE", "Jersey", "+44-1534", ResourceTable.Media_flag_je, "JEP"),
            new Country("JM", "Jamaica", "+1-876", ResourceTable.Media_flag_jm, "JMD"),
            new Country("JO", "Jordan", "+962", ResourceTable.Media_flag_jo, "JOD"),
            new Country("JP", "Japan", "+81", ResourceTable.Media_flag_jp, "JPY"),
            new Country("KE", "Kenya", "+254", ResourceTable.Media_flag_ke, "KES"),
            new Country("KG", "Kyrgyzstan", "+996", ResourceTable.Media_flag_kg, "KGS"),
            new Country("KH", "Cambodia", "+855", ResourceTable.Media_flag_kh, "KHR"),
            new Country("KI", "Kiribati", "+686", ResourceTable.Media_flag_ki, "AUD"),
            new Country("KM", "Comoros", "+269", ResourceTable.Media_flag_km, "KMF"),
            new Country("KN", "Saint Kitts and Nevis", "+1-869", ResourceTable.Media_flag_kn, "XCD"),
            new Country("KP", "North Korea", "+850", ResourceTable.Media_flag_kp, "KPW"),
            new Country("KR", "South Korea", "+82", ResourceTable.Media_flag_kr, "KRW"),
            new Country("KW", "Kuwait", "+965", ResourceTable.Media_flag_kw, "KWD"),
            new Country("KY", "Cayman Islands", "+1-345", ResourceTable.Media_flag_ky, "KYD"),
            new Country("KZ", "Kazakhstan", "+7", ResourceTable.Media_flag_kz, "KZT"),
            new Country("LA", "Laos", "+856", ResourceTable.Media_flag_la, "LAK"),
            new Country("LB", "Lebanon", "+961", ResourceTable.Media_flag_lb, "LBP"),
            new Country("LC", "Saint Lucia", "+1-758", ResourceTable.Media_flag_lc, "XCD"),
            new Country("LI", "Liechtenstein", "+423", ResourceTable.Media_flag_li, "CHF"),
            new Country("LK", "Sri Lanka", "+94", ResourceTable.Media_flag_lk, "LKR"),
            new Country("LR", "Liberia", "+231", ResourceTable.Media_flag_lr, "LRD"),
            new Country("LS", "Lesotho", "+266", ResourceTable.Media_flag_ls, "LSL"),
            new Country("LT", "Lithuania", "+370", ResourceTable.Media_flag_lt, "LTL"),
            new Country("LU", "Luxembourg", "+352", ResourceTable.Media_flag_lu, "EUR"),
            new Country("LV", "Latvia", "+371", ResourceTable.Media_flag_lv, "LVL"),
            new Country("LY", "Libya", "+218", ResourceTable.Media_flag_ly, "LYD"),
            new Country("MA", "Morocco", "+212", ResourceTable.Media_flag_ma, "MAD"),
            new Country("MC", "Monaco", "+377", ResourceTable.Media_flag_mc, "EUR"),
            new Country("MD", "Moldova", "+373", ResourceTable.Media_flag_md, "MDL"),
            new Country("ME", "Montenegro", "+382", ResourceTable.Media_flag_me, "EUR"),
            new Country("MF", "Saint Martin", "+590", ResourceTable.Media_flag_mf, "EUR"),
            new Country("MG", "Madagascar", "+261", ResourceTable.Media_flag_mg, "MGA"),
            new Country("MH", "Marshall Islands", "+692", ResourceTable.Media_flag_mh, "USD"),
            new Country("MK", "Macedonia", "+389", ResourceTable.Media_flag_mk,
                    "MKD"),
            new Country("ML", "Mali", "+223", ResourceTable.Media_flag_ml, "XOF"),
            new Country("MM", "Myanmar", "+95", ResourceTable.Media_flag_mm, "MMK"),
            new Country("MN", "Mongolia", "+976", ResourceTable.Media_flag_mn, "MNT"),
            new Country("MO", "Macao", "+853", ResourceTable.Media_flag_mo, "MOP"),
            new Country("MP", "Northern Mariana Islands", "+1-670", ResourceTable.Media_flag_mp, "USD"),
            new Country("MR", "Mauritania", "+222", ResourceTable.Media_flag_mr, "MRO"),
            new Country("MS", "Montserrat", "+1-664", ResourceTable.Media_flag_ms, "XCD"),
            new Country("MT", "Malta", "+356", ResourceTable.Media_flag_mt, "EUR"),
            new Country("MU", "Mauritius", "+230", ResourceTable.Media_flag_mu, "MUR"),
            new Country("MV", "Maldives", "+960", ResourceTable.Media_flag_mv, "MVR"),
            new Country("MW", "Malawi", "+265", ResourceTable.Media_flag_mw, "MWK"),
            new Country("MX", "Mexico", "+52", ResourceTable.Media_flag_mx, "MXN"),
            new Country("MY", "Malaysia", "+60", ResourceTable.Media_flag_my, "MYR"),
            new Country("MZ", "Mozambique", "+258", ResourceTable.Media_flag_mz, "MZN"),
            new Country("NA", "Namibia", "+264", ResourceTable.Media_flag_na, "NAD"),
            new Country("NC", "New Caledonia", "+687", ResourceTable.Media_flag_nc, "XPF"),
            new Country("NE", "Niger", "+227", ResourceTable.Media_flag_ne, "XOF"),
            new Country("NG", "Nigeria", "+234", ResourceTable.Media_flag_ng, "NGN"),
            new Country("NI", "Nicaragua", "+505", ResourceTable.Media_flag_ni, "NIO"),
            new Country("NL", "Netherlands", "+31", ResourceTable.Media_flag_nl, "EUR"),
            new Country("NO", "Norway", "+47", ResourceTable.Media_flag_no, "NOK"),
            new Country("NP", "Nepal", "+977", ResourceTable.Media_flag_np, "NPR"),
            new Country("NR", "Nauru", "+674", ResourceTable.Media_flag_nr, "AUD"),
            new Country("NU", "Niue", "+683", ResourceTable.Media_flag_nu, "NZD"),
            new Country("NZ", "New Zealand", "+64", ResourceTable.Media_flag_nz, "NZD"),
            new Country("OM", "Oman", "+968", ResourceTable.Media_flag_om, "OMR"),
            new Country("PA", "Panama", "+507", ResourceTable.Media_flag_pa, "PAB"),
            new Country("PE", "Peru", "+51", ResourceTable.Media_flag_pe, "PEN"),
            new Country("PF", "French Polynesia", "+689", ResourceTable.Media_flag_pf, "XPF"),
            new Country("PG", "Papua New Guinea", "+675", ResourceTable.Media_flag_pg, "PGK"),
            new Country("PH", "Philippines", "+63", ResourceTable.Media_flag_ph, "PHP"),
            new Country("PK", "Pakistan", "+92", ResourceTable.Media_flag_pk, "PKR"),
            new Country("PL", "Poland", "+48", ResourceTable.Media_flag_pl, "PLN"),
            new Country("PM", "Saint Pierre and Miquelon", "+508", ResourceTable.Media_flag_pm, "EUR"),
            new Country("PN", "Pitcairn", "+64", ResourceTable.Media_flag_pn, "NZD"),
            new Country("PR", "Puerto Rico", "+1-787, +1-939", ResourceTable.Media_flag_pr, "USD"),
            new Country("PS", "Palestinian", "+970", ResourceTable.Media_flag_ps, "ILS"),
            new Country("PT", "Portugal", "+351", ResourceTable.Media_flag_pt, "EUR"),
            new Country("PW", "Palau", "+680", ResourceTable.Media_flag_pw, "USD"),
            new Country("PY", "Paraguay", "+595", ResourceTable.Media_flag_py, "PYG"),
            new Country("QA", "Qatar", "+974", ResourceTable.Media_flag_qa, "QAR"),
            new Country("RE", "Reunion", "+262", ResourceTable.Media_flag_re, "EUR"),
            new Country("RO", "Romania", "+40", ResourceTable.Media_flag_ro, "RON"),
            new Country("RS", "Serbia", "+381", ResourceTable.Media_flag_rs, "RSD"),
            new Country("RU", "Russia", "+7", ResourceTable.Media_flag_ru, "RUB"),
            new Country("RW", "Rwanda", "+250", ResourceTable.Media_flag_rw, "RWF"),
            new Country("SA", "Saudi Arabia", "+966", ResourceTable.Media_flag_sa, "SAR"),
            new Country("SB", "Solomon Islands", "+677", ResourceTable.Media_flag_sb, "SBD"),
            new Country("SC", "Seychelles", "+248", ResourceTable.Media_flag_sc, "SCR"),
            new Country("SD", "Sudan", "+249", ResourceTable.Media_flag_sd, "SDG"),
            new Country("SE", "Sweden", "+46", ResourceTable.Media_flag_se, "SEK"),
            new Country("SG", "Singapore", "+65", ResourceTable.Media_flag_sg, "SGD"),
            new Country("SH", "Saint Helena", "+290", ResourceTable.Media_flag_sh,
                    "SHP"),
            new Country("SI", "Slovenia", "+386", ResourceTable.Media_flag_si, "EUR"),
            new Country("SJ", "Svalbard and Jan Mayen", "+47", ResourceTable.Media_flag_sj, "NOK"),
            new Country("SK", "Slovakia", "+421", ResourceTable.Media_flag_sk, "EUR"),
            new Country("SL", "Sierra Leone", "+232", ResourceTable.Media_flag_sl, "SLL"),
            new Country("SM", "San Marino", "+378", ResourceTable.Media_flag_sm, "EUR"),
            new Country("SN", "Senegal", "+221", ResourceTable.Media_flag_sn, "XOF"),
            new Country("SO", "Somalia", "+252", ResourceTable.Media_flag_so, "SOS"),
            new Country("SR", "Suriname", "+597", ResourceTable.Media_flag_sr, "SRD"),
            new Country("SS", "South Sudan", "+211", ResourceTable.Media_flag_ss, "SSP"),
            new Country("ST", "Sao Tome and Principe", "+239", ResourceTable.Media_flag_st, "STD"),
            new Country("SV", "El Salvador", "+503", ResourceTable.Media_flag_sv, "SVC"),
            new Country("SX", "Sint Maarten", "+1-721", ResourceTable.Media_flag_sx, "ANG"),
            new Country("SY", "Syria", "+963", ResourceTable.Media_flag_sy, "SYP"),
            new Country("SZ", "Swaziland", "+268", ResourceTable.Media_flag_sz, "SZL"),
            new Country("TC", "Turks and Caicos Islands", "+1-649", ResourceTable.Media_flag_tc, "USD"),
            new Country("TD", "Chad", "+235", ResourceTable.Media_flag_td, "XAF"),
            new Country("TG", "Togo", "+228", ResourceTable.Media_flag_tg, "XOF"),
            new Country("TH", "Thailand", "+66", ResourceTable.Media_flag_th, "THB"),
            new Country("TJ", "Tajikistan", "+992", ResourceTable.Media_flag_tj, "TJS"),
            new Country("TK", "Tokelau", "+690", ResourceTable.Media_flag_tk, "NZD"),
            new Country("TL", "East Timor", "+670", ResourceTable.Media_flag_tl, "USD"),
            new Country("TM", "Turkmenistan", "+993", ResourceTable.Media_flag_tm, "TMT"),
            new Country("TN", "Tunisia", "+216", ResourceTable.Media_flag_tn, "TND"),
            new Country("TO", "Tonga", "+676", ResourceTable.Media_flag_to, "TOP"),
            new Country("TR", "Turkey", "+90", ResourceTable.Media_flag_tr, "TRY"),
            new Country("TT", "Trinidad and Tobago", "+1-868", ResourceTable.Media_flag_tt, "TTD"),
            new Country("TV", "Tuvalu", "+688", ResourceTable.Media_flag_tv, "AUD"),
            new Country("TW", "Taiwan", "+886", ResourceTable.Media_flag_tw, "TWD"),
            new Country("TZ", "Tanzania", "+255", ResourceTable.Media_flag_tz, "TZS"),
            new Country("UA", "Ukraine", "+380", ResourceTable.Media_flag_ua, "UAH"),
            new Country("UG", "Uganda", "+256", ResourceTable.Media_flag_ug, "UGX"),
            new Country("US", "United States", "+1", ResourceTable.Media_flag_us, "USD"),
            new Country("UY", "Uruguay", "+598", ResourceTable.Media_flag_uy, "UYU"),
            new Country("UZ", "Uzbekistan", "+998", ResourceTable.Media_flag_uz, "UZS"),
            new Country("VA", "Vatican", "+379", ResourceTable.Media_flag_va, "EUR"),
            new Country("VC", "Saint Vincent and the Grenadines", "+1-784", ResourceTable.Media_flag_vc, "XCD"),
            new Country("VE", "Venezuela", "+58", ResourceTable.Media_flag_ve, "VEF"),
            new Country("VG", "British Virgin Islands", "+1-284", ResourceTable.Media_flag_vg, "USD"),
            new Country("VI", "U.S. Virgin Islands", "+1-340", ResourceTable.Media_flag_vi, "USD"),
            new Country("VN", "Vietnam", "+84", ResourceTable.Media_flag_vn, "VND"),
            new Country("VU", "Vanuatu", "+678", ResourceTable.Media_flag_vu, "VUV"),
            new Country("WF", "Wallis and Futuna", "+681", ResourceTable.Media_flag_wf, "XPF"),
            new Country("WS", "Samoa", "+685", ResourceTable.Media_flag_ws, "WST"),
            new Country("XK", "Kosovo", "+383", ResourceTable.Media_flag_xk, "EUR"),
            new Country("YE", "Yemen", "+967", ResourceTable.Media_flag_ye, "YER"),
            new Country("YT", "Mayotte", "+262", ResourceTable.Media_flag_yt, "EUR"),
            new Country("ZA", "South Africa", "+27", ResourceTable.Media_flag_za, "ZAR"),
            new Country("ZM", "Zambia", "+260", ResourceTable.Media_flag_zm, "ZMW"),
            new Country("ZW", "Zimbabwe", "+263", ResourceTable.Media_flag_zw, "USD"),
    };
    // endregion

    public static final int SORT_BY_NONE = 0;
    public static final int SORT_BY_NAME = 1;
    public static final int SORT_BY_ISO = 2;
    public static final int SORT_BY_DIAL_CODE = 3;
    public static final int THEME_OLD = 1;
    public static final int THEME_NEW = 2;

    private int theme;
    private int style;
    private Context context;
    private int sortBy = SORT_BY_NONE;
    private OnCountryPickerListener onCountryPickerListener;
    private boolean canSearch = true;
    private List<Country> countries;
    private TextField searchEditText;
    private ListContainer countriesRecyclerView;
    private DirectionalLayout rootView;
    private int textColor;
    private int hintColor;
    private int backgroundColor;
    private int searchIconId;
    private Element searchIcon;
    private CountriesAdapter adapter;
    private List<Country> searchResults;
    //    private BottomSheetDialogView bottomSheetDialog;
    private CommonDialog dialog;

    private CountryPicker() {
    }

    CountryPicker(Builder builder) {
        sortBy = builder.sortBy;
        if (builder.onCountryPickerListener != null) {
            onCountryPickerListener = builder.onCountryPickerListener;
        }
        style = builder.style;
        context = builder.context;
        canSearch = builder.canSearch;
        theme = builder.theme;
        countries = new ArrayList<>(Arrays.asList(COUNTRIES));
        sortCountries(countries);
    }

    @Override
    public void onStateChanged(Lifecycle.Event event, Intent intent) {
        switch (event) {
            case ON_BACKGROUND:
                dismissDialogs();
                break;
        }
    }

    private void sortCountries(List<Country> countries) {
        if (sortBy == SORT_BY_NAME) {
            Collections.sort(countries, new Comparator<Country>() {
                @Override
                public int compare(Country country1, Country country2) {
                    return country1.getName().trim().compareToIgnoreCase(country2.getName().trim());
                }
            });
        } else if (sortBy == SORT_BY_ISO) {
            Collections.sort(countries, new Comparator<Country>() {
                @Override
                public int compare(Country country1, Country country2) {
                    return country1.getCode().trim().compareToIgnoreCase(country2.getCode().trim());
                }
            });
        } else if (sortBy == SORT_BY_DIAL_CODE) {
            Collections.sort(countries, new Comparator<Country>() {
                @Override
                public int compare(Country country1, Country country2) {
                    return country1.getDialCode().trim().compareToIgnoreCase(country2.getDialCode().trim());
                }
            });
        }
    }

    public void showDialog(Ability activity) {
        if (countries == null || countries.isEmpty()) {
            throw new IllegalArgumentException(context.getString(ResourceTable.String_error_no_countries_found));
        } else {
            activity.getLifecycle().addObserver(this);
            dialog = new CommonDialog(activity);
            Component dialogView = LayoutScatter.getInstance(activity)
                                           .parse(ResourceTable.Layout_country_picker, null, true);
            initiateUi(dialogView);
            setCustomStyle(dialogView);
            setSearchEditText();
            setupRecyclerView(dialogView);
            dialog.setContentCustomComponent(dialogView);
            if (dialog.getWindow() != null) {
                WindowManager.LayoutConfig params = dialog.getWindow().getLayoutConfig().get();
                params.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
                params.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
                dialog.getWindow().setLayoutConfig(params);
                if (theme == THEME_NEW) {
                    ShapeElement shapeElement = new ShapeElement(activity, ResourceTable.Graphic_ic_dialog_new_background);
                    dialog.setCornerRadius(AttrHelper.vp2px(8, activity));
                    if (shapeElement != null) {
                        shapeElement.setStateColorMode(BlendMode.SRC_ATOP);
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
                    }
                    rootView.setBackground(shapeElement);
                    dialog.getWindow().setBackgroundColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
                }
            }
            dialog.show();
        }
    }

    private void dismissDialogs() {
        if (dialog != null) {
            dialog.destroy();
        }
    }

    @Override
    public void setupRecyclerView(Component sheetView) {
        searchResults = new ArrayList<>();
        searchResults.addAll(countries);
        sortCountries(searchResults);
        adapter = new CountriesAdapter(context, searchResults,
                new OnItemClickListener() {
                    @Override
                    public void onItemClicked(Country country) {
                        if (onCountryPickerListener != null) {
                            onCountryPickerListener.onSelectCountry(country);
                            if (dialog != null) {
                                dialog.destroy();
                            }
                            dialog = null;
                            textColor = 0;
                            hintColor = 0;
                            backgroundColor = 0;
                            searchIconId = 0;
                            searchIcon = null;
                        }
                    }
                },
                textColor);
        countriesRecyclerView.setItemProvider(adapter);
    }

    @Override
    public void setSearchEditText() {
        if (canSearch) {
            searchEditText.addTextObserver(new Text.TextObserver() {
                @Override
                public void onTextUpdated(String s, int i, int i1, int i2) {
                    search(s);
                }
            });
        } else {
            searchEditText.setVisibility(Component.HIDE);
        }
    }

    private void search(String searchQuery) {
        searchResults.clear();
        for (Country country : countries) {
            if (country.getName().toLowerCase(Locale.ENGLISH).contains(searchQuery.toLowerCase())) {
                searchResults.add(country);
            }
        }
        sortCountries(searchResults);
        adapter.notifyDataChanged();
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void setCustomStyle(Component sheetView) {
        if (style != 0) {
            textColor = Color.WHITE.getValue();
            hintColor = Color.WHITE.getValue();
            backgroundColor = Color.getIntColor("#424242");
            searchIconId = ResourceTable.Graphic_ic_dummy_icon;
            searchEditText.setTextColor(new Color(textColor));
            searchEditText.setHintColor(new Color(hintColor));
            searchIcon = new VectorElement(context, searchIconId);
            searchEditText.setAroundElements(searchIcon, null, null, null);
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
            rootView.setBackground(shapeElement);
        }
    }

    @Override
    public void initiateUi(Component sheetView) {
        searchEditText = (TextField) sheetView.findComponentById(ResourceTable.Id_country_code_picker_search);
        countriesRecyclerView = (ListContainer) sheetView.findComponentById(ResourceTable.Id_countries_recycler_view);
        rootView = (DirectionalLayout) sheetView.findComponentById(ResourceTable.Id_rootView);
    }

    public Country getCountryFromSIM() {
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        int slotId = -1;
        for (int i = 0; i < 3; i++) {
            if (simInfoManager.isSimActive(i)) {
                slotId = i;
                break;
            }
        }
        if (simInfoManager != null
                    && simInfoManager.getSimState(slotId) != TelephonyConstants.SIM_STATE_NOT_PRESENT) {
            return getCountryByISO(simInfoManager.getIsoCountryCodeForSim(slotId));
        }
        return null;
    }

    public Country getCountryByLocale(Locale locale) {
        String countryIsoCode = locale.getCountry();
        return getCountryByISO(countryIsoCode);
    }

    public Country getCountryByName(String countryName) {
        Collections.sort(countries, new NameComparator());
        Country country = new Country();
        country.setName(countryName);
        int i = Collections.binarySearch(countries, country, new NameComparator());
        if (i < 0) {
            return null;
        } else {
            return countries.get(i);
        }
    }

    public Country getCountryByISO(String countryIsoCode) {
        Collections.sort(countries, new ISOCodeComparator());
        Country country = new Country();
        country.setCode(countryIsoCode);
        int i = Collections.binarySearch(countries, country, new ISOCodeComparator());
        if (i < 0) {
            return null;
        } else {
            return countries.get(i);
        }
    }

    public Country getCountryByDialCode(String dialCode) {
        Collections.sort(countries, new DialCodeComparator());
        Country country = new Country();
        country.setDialCode(dialCode);
        int i = Collections.binarySearch(countries, country, new DialCodeComparator());
        if (i < 0) {
            return null;
        } else {
            return countries.get(i);
        }
    }

    public static class Builder {
        private Context context;
        private int sortBy = SORT_BY_NONE;
        private boolean canSearch = true;
        private OnCountryPickerListener onCountryPickerListener;
        private int style;
        private int theme = THEME_NEW;

        public Builder with(Context context) {
            this.context = context;
            return this;
        }

        public Builder style(int style) {
            this.style = style;
            return this;
        }

        public Builder sortBy(int sortBy) {
            this.sortBy = sortBy;
            return this;
        }

        public Builder listener(OnCountryPickerListener onCountryPickerListener) {
            this.onCountryPickerListener = onCountryPickerListener;
            return this;
        }

        public Builder canSearch(boolean canSearch) {
            this.canSearch = canSearch;
            return this;
        }

        public Builder theme(int theme) {
            this.theme = theme;
            return this;
        }

        public CountryPicker build() {
            return new CountryPicker(this);
        }
    }

    private static class ISOCodeComparator implements Comparator<Country>, Serializable {
        @Override
        public int compare(Country country, Country nextCountry) {
            return country.getCode().compareToIgnoreCase(nextCountry.getCode());
        }
    }

    private static class NameComparator implements Comparator<Country>, Serializable {
        @Override
        public int compare(Country country, Country nextCountry) {
            return country.getName().compareToIgnoreCase(nextCountry.getName());
        }
    }

    public static class DialCodeComparator implements Comparator<Country>, Serializable {
        @Override
        public int compare(Country country, Country nextCountry) {
            return country.getDialCode().compareTo(nextCountry.getDialCode());
        }
    }
}
