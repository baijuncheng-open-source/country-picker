package com.mukesh.countrypicker;

import com.mukesh.countrypicker.listeners.OnItemClickListener;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.IOException;
import java.util.List;

public class CountriesAdapter extends BaseItemProvider {
    private static final String TAG = "CountriesAdapter";

    private OnItemClickListener listener;
    private List<Country> countries;
    private Context context;
    private int textColor;

    public CountriesAdapter(Context context, List<Country> countries,
                            OnItemClickListener listener, int textColor) {
        this.context = context;
        this.countries = countries;
        this.listener = listener;
        this.textColor = textColor;
    }


    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int i) {
        return countries.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
        final Country country = countries.get(position);
        CommonHolder holder = CommonHolder.get(context, convertView, componentContainer,
                ResourceTable.Layout_item_country, position);
        Image countryFlagImageView = holder.getView(ResourceTable.Id_country_flag);
        Text countryNameText = holder.getView(ResourceTable.Id_country_title);
        DirectionalLayout rootView = holder.getView(ResourceTable.Id_rootView);
        countryNameText.setText(country.getName());
        countryNameText.setTextColor(textColor == 0 ? Color.BLACK : new Color(textColor));
        country.loadFlagByCode(context);
        if (country.getFlag() != -1) {
            try {
                PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(country.getFlag()));
                countryFlagImageView.setImageElement(pixelMapElement);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
        }
        rootView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listener.onItemClicked(country);
            }
        });
        return holder.getConvertView();
    }
}
