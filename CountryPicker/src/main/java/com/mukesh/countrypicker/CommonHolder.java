/*
 * All rights Reserved, Designed By www.bill-jc.com
 *
 * @Title: ${animewallpaper}
 *
 * @Package ${com.github.miao1007.animewallpaper}
 *
 * @Description: ${Provide loading function interface)
 *
 * @author: baijuncheng
 *
 * @date: ${2021} ${4.21}
 *
 *  @version V1.1.1
 *
 * @Copyright: ${2021} bill-jc.com Inc. All rights reserved.
 */

package com.mukesh.countrypicker;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.HashMap;

public class CommonHolder {
    private final HashMap<Integer, Component> mViews;
    private Component mConvertView;

    private CommonHolder(Context context, ComponentContainer parent, int layoutId,
                         int position) {
        this.mViews = new HashMap<>();
        mConvertView = LayoutScatter.getInstance(context).parse(layoutId, parent,
                false);
        //setTag
        mConvertView.setTag(this);
    }

    /**
     * 拿到一个ViewHolder对象
     * @param context
     * @param convertView
     * @param parent
     * @param layoutId
     * @param position
     * @return
     */
    public static CommonHolder get(Context context, Component convertView, ComponentContainer parent,
                                   int layoutId, int position) {
        if (convertView == null) {
            return new CommonHolder(context, parent, layoutId, position);

        }
        return (CommonHolder) convertView.getTag();
    }


    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     * @param componentId
     * @return
     */
    public <T extends Component> T getView(int componentId) {

        Component component = mViews.get(componentId);
        if (component == null) {
            component = mConvertView.findComponentById(componentId);
            mViews.put(componentId, component);
        }
        return (T) component;
    }

    public Component getConvertView() {
        return mConvertView;
    }
}
