/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mukesh.countrypicker;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.logging.Logger;

public class AttrUtil {
    private static final String TAG = "AttrUtil";
    private final AttrSet attrSet;

    public AttrUtil(AttrSet attrSet) {
        this.attrSet = attrSet;
    }

    public String getStringValue(String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * getDimension
     *
     * @param key
     * @param defValue
     * @return
     */
    public float getFloatValue(String key, float defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    public int getDimensionValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getDimensionValue();
        } else {
            return defValue;
        }
    }

    /**
     * getInt、getDimensionPixelSize
     *
     * @param key
     * @param defValue
     * @return
     */
    public int getIntegerValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * getColor
     *
     * @param key
     * @param defValue
     * @return
     */
    public int getColorValue(String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

    public Color getColorValue(String key, Color defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * getBoolean
     *
     * @param key
     * @param defValue
     * @return
     */
    public boolean getBooleanValue(String key, boolean defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    public Element getElementValue(String key, Element defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getElement();
        } else {
            return defValue;
        }
    }

    public boolean hasValue(String key) {
        return attrSet.getAttr(key).isPresent();
    }

    /**
     * 根据资源名称获取资源 id
     * <p>
     * <p>
     * 不提倡使用这个方法获取资源,比其直接获取ID效率慢
     * <p>
     * <p>
     * 例如
     * getResources().getIdentifier("ic_launcher", "drawable", getPackageName());
     *
     * @param name
     * @param defType
     * @return
     */
    public static int getResIdByName(String name, String defType) {
        Field[] fields = ResourceTable.class.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName().toLowerCase();
            int index = fieldName.indexOf("_");
            String type = fieldName.substring(0, index);
            String n = fieldName.substring(index + 1);
            if (type.equals(defType.toLowerCase()) && n.equals(name.toLowerCase())) {
                try {
                    int resId = field.getInt(ResourceTable.class);
                    return resId;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
        return -1;
    }

    public static int getResIdByAttrSet(AttrSet attrs, String attrName, int defValue) {
        int resId = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            String src = attrs.getAttr(attrName).get().getStringValue();
            resId = Integer.parseInt(src.substring(src.indexOf(":") + 1));
            Logger.getLogger(TAG).info("getResIdByAttrSet: " + resId);
        }
        return defValue;
    }

    public static String[] getStringArrayAttrValue(Context context, AttrSet attrs, String attrName) {
        String attrValue = getStringAttrValue(attrs, attrName, null);
        String[] split = attrValue.split(":");
        int resid = getResIdByName(split[1], "strarray");
        if (resid != -1) {
            try {
                String[] result = context.getResourceManager().getElement(resid).getStringArray();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getStringAttrValue(AttrSet attrs, String attrName, String defValue) {
        String result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getStringValue();
        }
        return result;
    }

    public static int getColorByRes (Context context, int resId, int defColor) {
        try {
            int color = context.getResourceManager().getElement(resId).getColor();
            return color;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return defColor;
    }
}
