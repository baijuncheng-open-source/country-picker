package com.mukeshsolanki.countrypickerexample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class ResultActivity extends Ability {
    private static final String TAG = "ResultActivity";

    public static final String BUNDLE_KEY_COUNTRY_NAME = "country_name";
    public static final String BUNDLE_KEY_COUNTRY_ISO = "country_iso";
    public static final String BUNDLE_KEY_COUNTRY_DIAL_CODE = "dial_code";
    public static final String BUNDLE_KEY_COUNTRY_CURRENCY = "currency";
    public static final String BUNDLE_KEY_COUNTRY_FLAG_IMAGE = "flag_image";

    private Text countryNameTextView, countryIsoCodeTextView, countryDialCodeTextView,
            selectedCountryCurrency;
    private Image countryFlagImageView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_result);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#212121"));
        initialize();
        showData(intent);
    }

    private void showData(Intent intent) {
        countryNameTextView.setText(intent.getStringParam(BUNDLE_KEY_COUNTRY_NAME));
        countryIsoCodeTextView.setText(intent.getStringParam(BUNDLE_KEY_COUNTRY_ISO));
        countryDialCodeTextView.setText(intent.getStringParam(BUNDLE_KEY_COUNTRY_DIAL_CODE));
        selectedCountryCurrency.setText(intent.getStringParam(BUNDLE_KEY_COUNTRY_CURRENCY));
        countryFlagImageView.setPixelMap(intent.getIntParam(BUNDLE_KEY_COUNTRY_FLAG_IMAGE, -1));
    }

    private void initialize() {
        countryNameTextView = (Text) findComponentById(ResourceTable.Id_selected_country_name_text_view);
        countryIsoCodeTextView = (Text) findComponentById(ResourceTable.Id_selected_country_iso_text_view);
        countryDialCodeTextView = (Text) findComponentById(ResourceTable.Id_selected_country_dial_code_text_view);
        countryFlagImageView = (Image) findComponentById(ResourceTable.Id_selected_country_flag_image_view);
        selectedCountryCurrency = (Text) findComponentById(ResourceTable.Id_selected_country_currency);
        Image backButton = (Image) findComponentById(ResourceTable.Id_back_button);
        backButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
    }
}
