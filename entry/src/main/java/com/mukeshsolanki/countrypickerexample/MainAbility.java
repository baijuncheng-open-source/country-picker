package com.mukeshsolanki.countrypickerexample;

import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.listeners.OnCountryPickerListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.InputAttribute;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.Switch;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;

import java.util.Locale;
import java.util.logging.Logger;

public class MainAbility extends Ability implements OnCountryPickerListener {
    private static final String TAG = "MainAbility";
    private int CUSTOM_PICKER_STYLE = 1;
    private Button pickCountryButton, findByNameButton, findBySimButton, findByLocaleButton,
            findByIsoButton;
    private CountryPicker countryPicker;
    private Switch themeSwitch, styleSwitch, searchSwitch;
    private RadioContainer sortByRadioGroup;
    private int sortBy = CountryPicker.SORT_BY_NONE;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#212121"));
        initialize();
        setListener();
    }

    private void initialize() {
        pickCountryButton = (Button) findComponentById(ResourceTable.Id_country_picker_button);
        themeSwitch = (Switch) findComponentById(ResourceTable.Id_theme_toggle_switch);
        styleSwitch = (Switch) findComponentById(ResourceTable.Id_custom_style_toggle_switch);
        sortByRadioGroup = (RadioContainer) findComponentById(ResourceTable.Id_sort_by_radio_group);
        searchSwitch = (Switch) findComponentById(ResourceTable.Id_search_switch);
        findByNameButton = (Button) findComponentById(ResourceTable.Id_by_name_button);
        findBySimButton = (Button) findComponentById(ResourceTable.Id_by_sim_button);
        findByLocaleButton = (Button) findComponentById(ResourceTable.Id_by_local_button);
        findByIsoButton = (Button) findComponentById(ResourceTable.Id_by_iso_button);
    }

    private void setListener() {
        findByNameButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                findByName();
            }
        });
        findBySimButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                findBySim();
            }
        });
        findByLocaleButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                findByLocale();
            }
        });
        findByIsoButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                findByIson();
            }
        });
        sortByRadioGroup.setMarkChangedListener(
                new RadioContainer.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(RadioContainer radioContainer, int id) {
                        Logger.getLogger(TAG).info("MarkedButtonId:" + radioContainer.getMarkedButtonId());
                        switch (id) {
                            case 0:
                                sortBy = CountryPicker.SORT_BY_NONE;
                                break;
                            case 1:
                                sortBy = CountryPicker.SORT_BY_NAME;
                                break;
                            case 2:
                                sortBy = CountryPicker.SORT_BY_DIAL_CODE;
                                break;
                            case 3:
                                sortBy = CountryPicker.SORT_BY_ISO;
                                break;
                        }
                    }
                });
        pickCountryButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showPicker();
            }
        });
    }

    private void findByIson() {
        showTextFieldDialog("Country ISO Code", 1);
    }

    private void findByLocale() {
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();
        Country country = countryPicker.getCountryByLocale(Locale.getDefault());
        if (country == null) {
            new ToastDialog(this).setText("Country not found").show();
        } else {
            showResultActivity(country);
        }
    }

    private void findBySim() {
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();
        Country country = countryPicker.getCountryFromSIM();
        if (country == null) {
            new ToastDialog(this).setText("Country not found").show();
        } else {
            showResultActivity(country);
        }
    }

    private void findByName() {
        showTextFieldDialog("Country Name", 0);
    }

    private void showResultActivity(Country country) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                                      .withDeviceId("")
                                      .withBundleName(getBundleName())
                                      .withAbilityName(ResultActivity.class.getName())
                                      .build();
        intent.setOperation(operation);
        intent.setParam(ResultActivity.BUNDLE_KEY_COUNTRY_NAME, country.getName());
        intent.setParam(ResultActivity.BUNDLE_KEY_COUNTRY_CURRENCY, country.getCurrency());
        intent.setParam(ResultActivity.BUNDLE_KEY_COUNTRY_DIAL_CODE, country.getDialCode());
        intent.setParam(ResultActivity.BUNDLE_KEY_COUNTRY_ISO, country.getCode());
        intent.setParam(ResultActivity.BUNDLE_KEY_COUNTRY_FLAG_IMAGE, country.getFlag());
        startAbility(intent);
    }

    private void showPicker() {
        CountryPicker.Builder builder = new CountryPicker.Builder().with(this).listener(this);
        if (styleSwitch.isChecked()) {
            builder.style(CUSTOM_PICKER_STYLE);
        }
        builder.theme(themeSwitch.isChecked() ? CountryPicker.THEME_NEW : CountryPicker.THEME_OLD);
        builder.canSearch(searchSwitch.isChecked());
        builder.sortBy(sortBy);
        countryPicker = builder.build();
//        if (useBottomSheet.isChecked()) {
//            countryPicker.showBottomSheet(this);
//        } else {
        countryPicker.showDialog(this);
//        }
    }

    @Override
    public void onSelectCountry(Country country) {
        Logger.getLogger(TAG).info("countries:" + country);
        showResultActivity(country);
    }

    private void showTextFieldDialog(String title, int searchType) {
        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();
        CommonDialog builder = new CommonDialog(this);
        builder.setCornerRadius(0);
        builder.setSize(AttrHelper.vp2px(360, this), ComponentContainer.LayoutConfig.MATCH_CONTENT);
        TextField input = new TextField(this);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, AttrHelper.fp2px(20, this));
        layoutConfig.setMargins(AttrHelper.fp2px(10, this),
                AttrHelper.fp2px(10, this),
                AttrHelper.fp2px(10, this),
                AttrHelper.fp2px(10, this));
        input.setLayoutConfig(layoutConfig);
        input.setTextSize(AttrHelper.fp2px(16, this));
        ShapeElement basementElement = new ShapeElement();
        basementElement.setRgbColor(RgbColor.fromArgbInt(Color.BLUE.getValue()));
        input.setBasement(basementElement);
        input.setMultipleLine(false);
        input.setInputMethodOption(InputAttribute.ENTER_KEY_TYPE_GO | InputAttribute.PATTERN_TEXT);
        input.setFocusable(Component.FOCUS_ENABLE);
        input.requestFocus();
        builder.setContentCustomComponent(input);
        builder.siteRemovable(false);
        builder.setTitleText(title);
        builder.setButton(0, "Cancel", new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                iDialog.destroy();
            }
        });
        builder.setButton(1, "OK", new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                Country country;
                if (searchType == 1) {
                    country = countryPicker.getCountryByISO(input.getText());
                } else {
                    country = countryPicker.getCountryByName(input.getText());
                }
                if (country == null) {
                    new ToastDialog(MainAbility.this).setText("Country not found").show();
                } else {
                    builder.destroy();
                    showResultActivity(country);
                }
            }
        });
        builder.show();
    }
}
