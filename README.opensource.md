[
    {
        "Name": "CountryPicker",
        "License": "Apache License Version 2.0",
        "License File": "LICENSE.txt",
        "Version Number": "1.0",
        "Upstream URL": "https://gitee.com/baijuncheng-open-source/country-picker",
        "Description": "CountryPicker is a simple library that can be show a country picker. See the example to see more detail."
    }
]