# CountryPicker

#### 
CountryPicker is a simple library that can be show a country picker. See the example to see more detail.

## Preview
![image](images/image.gif)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/112800_641933e6_8230582.jpeg "Screenshot_20210526_112510_com.mukeshsolanki.countrypickerexample.jpg")

### Using CountryPicker Library in your application

#### Step 1: 
Compile the project and copy the HAR package generated in the Build directory of the CountryPicker folder into the project lib folder 

#### Step 2: 
Add the following code inside Gradle of Entry (just Demo)

 ```
 implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 ```
#### Step 3: 
remote maven repo integration

1.add mavenCentral dependency at repositories in your project build.gradle

```
allprojects {
    repositories {
        mavenCentral()
    }
}
```

2.add library dependency at your module build.gradle

```xml
implementation 'com.gitee.baijuncheng-open-source:country-picker:1.0.0'
```

### Table of content
- Getting Started
- Basic Usage
    - Dialog
    - CountryPicker methods
- Sorting
    - Sort by Name
    - Sort by ISO
    - Sort by Dial Code
- Searching
- Styling
    - Creating a Custom Style
    - Applying the style
- Theme
    - Material Design 1
    - Material Design 2

### Contribution
Bug reports and pull requests are welcome.
Make sure you use square/java-code-styles to format your code.

### License

```
MIT License

Copyright (c) 2018 Mukesh Solanki

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
